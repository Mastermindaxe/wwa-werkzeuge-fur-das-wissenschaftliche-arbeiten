% Christoph Hoffmeyer   - 664349
% Florian Nagel         - 661685

function [E] = envel(relDuration, fullDuration, fs)
%ENVEL Summary of this function goes here
%   Detailed explanation goes here

d = fullDuration * relDuration;
Ts = 1/fs;
t = 0:Ts:d;

% approximate height of the ADSR graph and dynamic adjustment to the tone
astep = 1/(0.15 * length(t));
bstep = 0.3/(0.05 * length(t));
sstep = 0.2/(0.7 * length(t));
rstep = 0.5/(0.1 * length(t));
A = [0:astep:1];
D = [1:-bstep:0.7];
S= [0.7:-sstep:0.5];
R = [0.5:-rstep:0];

E = [A,D,S,R];
E = E(1:length(E)-3);

end

