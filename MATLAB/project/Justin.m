%Justin H?bner 661135

clear all;

fs = 8000;
Ts = 1/fs;


%e)
%Noten der Treble Melodie
takt1treble = [56, 1/16; 55, 1/16];
takt2treble = [56, 1/16; 55, 1/16; 56, 1/16; 51, 1/16; 54, 1/16; 52, 1/16];
takt3treble = [49, 1/8; 0, 1/16; 40, 1/16; 44, 1/16; 49, 1/16];
takt4treble = [51, 1/8; 0, 1/16; 44, 1/16; 48, 1/16; 51, 1/16];
takt5treble = [52, 1/8; 0, 1/16; 44, 1/16; 56, 1/16; 55, 1/16];

trebleNoten = [takt1treble; takt2treble; takt3treble; takt4treble; takt5treble];
anztreb = size(trebleNoten);
treble = [];

for m = 1:anztreb(1)
    notentupel = trebleNoten(m, :);
    treble = [treble, note(notentupel(1), notentupel(2), 4, fs)];
end

%Noten des Bass
takt1bass = [0, 1/16; 0, 1/16];
takt2bass = [0, 1/16; 0, 1/16; 0, 1/16; 0, 1/16; 0, 1/16; 0, 1/16];
takt3bass = [25, 1/16; 32, 1/16; 37, 1/16; 0, 1/16; 0, 1/8];
takt4bass = [20, 1/16; 32, 1/16; 36, 1/16; 0, 1/16; 0, 1/8];
takt5bass = [25, 1/16; 32, 1/16; 37, 1/16; 0, 1/16; 0, 1/8];

bassNoten = [takt1bass; takt2bass; takt3bass; takt4bass; takt5bass];
anzbass = size(bassNoten);
bass = [];

for m = 1:anzbass(1)
    notentupel = bassNoten(m, :);
    bass = [bass, note(notentupel(1), notentupel(2), 4, fs)];
end

%Abspielen der Verktoraddition von Treble und Bass
sound(treble+bass, fs);


function E = envel(relDuration, fullDuration, fs)
d = fullDuration * relDuration;
Ts = 1/fs;
t = [0:Ts:d];
    
    %gesch?tze l?ngen/h?hen der ADSR Kurve und anpassen an den jeweiligen
    %Ton
    astep = 1/(0.15 * length(t));
    bstep = 0.3/(0.05 * length(t));
    sstep = 0.2/(0.7 * length(t));
    rstep = 0.5/(0.1 * length(t));
    A = [0:astep:1];
    D = [1:-bstep:0.7];
    S= [0.7:-sstep:0.5];
    R = [0.5:-rstep:0];

E = [A,D,S,R];
E = E(1:length(E)-3);
end

function tone = note(keynum, relDuration, fullDuration, fs)
r = nthroot(2,12);
frq = 440 * r^(keynum-49);
if keynum == 0
    tone = createWaveform(frq, fs, relDuration*fullDuration, 0);
else
    tone = createWaveform(frq, fs, relDuration*fullDuration, 0.1);
end

E = envel(relDuration, fullDuration, fs);
tone = tone.*E; %Nutzen der Huellkurve
end


function [sinusoid] = createWaveform(frequency, fs, duration, A)
    phi = 2*pi*rand(1);
    Ts=1/fs;
    t = [0:Ts:duration];
    sinusoid = A*cos(2*pi*frequency*t + phi);
end