% Christoph Hoffmeyer   - 664349
% Florian Nagel         - 661685

a = [40,42,44,45,47,49,51];
a = [a,fliplr(a)];

b = [];
for C = 1:length(a)
    b = [b, note(a(C), 1/4, 2, 8000)];
end
sound(0.5.*b, 8000);