% Christoph Hoffmeyer   - 664349
% Florian Nagel         - 661685

f1 = createWaveform(440, 8000, 2, 0.2);
f2 = createWaveform(1000, 8000, 2, 0.2);

fc = note(40, 1/4, 2, 8000);
fd = note(42, 1/4, 2, 8000);
fe = note(44, 1/4, 2, 8000);

sound(0.5.*[fc, fd, fe, f1, f2], 8000);