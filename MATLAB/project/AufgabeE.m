% Christoph Hoffmeyer   - 664349
% Florian Nagel         - 661685

fs = 8000;

% treble tones
beat1treble = [56, 1/16; 55, 1/16];
beat2treble = [56, 1/16; 55, 1/16; 56, 1/16; 51, 1/16; 54, 1/16; 52, 1/16];
beat3treble = [49, 1/8; 0, 1/16; 40, 1/16; 44, 1/16; 49, 1/16];
beat4treble = [51, 1/8; 0, 1/16; 44, 1/16; 48, 1/16; 51, 1/16];
beat5treble = [52, 1/8; 0, 1/16; 44, 1/16; 56, 1/16; 55, 1/16];

trebleTones = [beat1treble; beat2treble; beat3treble; beat4treble; beat5treble];
sizeTreble = size(trebleTones);
treble = [];

for m = 1:sizeTreble(1)
    toneTuple = trebleTones(m, :);
    treble = [treble, note(toneTuple(1), toneTuple(2), 4, fs)];
end

% bass tones
beat1bass = [0, 1/16; 0, 1/16];
beat2bass = [0, 1/16; 0, 1/16; 0, 1/16; 0, 1/16; 0, 1/16; 0, 1/16];
beat3bass = [25, 1/16; 32, 1/16; 37, 1/16; 0, 1/16; 0, 1/8];
beat4bass = [20, 1/16; 32, 1/16; 36, 1/16; 0, 1/16; 0, 1/8];
beat5bass = [25, 1/16; 32, 1/16; 37, 1/16; 0, 1/16; 0, 1/8];

bassTones = [beat1bass; beat2bass; beat3bass; beat4bass; beat5bass];
sizeBass = size(bassTones);
bass = [];

for m = 1:sizeBass(1)
    toneTuple = bassTones(m, :);
    bass = [bass, note(toneTuple(1), toneTuple(2), 4, fs)];
end

% play treble and bass together
sound(treble+bass, fs);