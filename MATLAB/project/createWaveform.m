% Christoph Hoffmeyer   - 664349
% Florian Nagel         - 661685

function [sinusoid] = createWaveform(frequency, fs, duration, A)
%CREATEWAVEFORM Summary of this function goes here
%   Detailed explanation goes here

phi = rand*2*pi;
t = [0:1/fs:duration];

%sinusoid( 1:fs:duration ) = A*cos(2*pi*frequency*duration*(1/fs) + phi);
%for R = 1:length(sinusoid)
    sinusoid = A*cos(2*pi*frequency*t + phi);
%end

end

