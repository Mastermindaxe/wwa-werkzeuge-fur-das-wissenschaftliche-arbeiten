% Christoph Hoffmeyer   - 664349
% Florian Nagel         - 661685

function [tone] = note(keynum, relDuration, fullDuration, fs)
%NOTE Summary of this function goes here
%   Detailed explanation goes here

% frequency = 440*(nthroot(2,12)^(keynum-49));
% 
% if keynum == 0
%     tone = createWaveform( frequency, fs, relDuration*fullDuration, 0 );
% else
%     tone = createWaveform( frequency, fs, relDuration*fullDuration, 0.1 );
% end
% 
% E = envel(relDuration, fullDuration, fs);
% tone = tone.*E;
r = nthroot(2,12);
frequency = 440 * r^(keynum-49);
if keynum == 0
    tone = createWaveform(frequency, fs, relDuration*fullDuration, 0);
else
    tone = createWaveform(frequency, fs, relDuration*fullDuration, 0.1);
end

E = envel(relDuration, fullDuration, fs);
tone = tone.*E; %Nutzen der Huellkurve
end

