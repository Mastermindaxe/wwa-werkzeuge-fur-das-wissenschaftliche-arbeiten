% Christoph Hoffmeyer   - 664349
% Florian Nagel         - 661685

function [ matrix ] = aufgabe2b( matrix, x )
%AUFGABE2A Summary of this function goes here
%   Detailed explanation goes here

for m = 1:numel(matrix)
    if matrix(m) < 0
        matrix(m) = x;
    end
end

end