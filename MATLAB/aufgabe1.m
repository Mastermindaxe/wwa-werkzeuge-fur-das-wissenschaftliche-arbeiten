% Christoph Hoffmeyer   - 664349
% Florian Nagel         - 661685

%a
N = 5;
k = 3;
a = 1:N;
a = [1;2;3;4;5];
a = linspace(1, 5, N);
a = ones(N, 1);
a = cumsum(a);

%b
b = a.';

%c
mtimes(a, b)

%d
A = a*b;

%e
c = b.*b;

%f
d = diag(A);
isequal(c, d)

%g
B11 = ones(2,3);
B12 = repmat(2,2);
B21 = repmat(3, 3, 2);
B22 = repmat(4,3);
B13 = repmat(5,5,1);

B = [B11,B12;B21,B22];
B = [B, B13];

%h
C = B(2:4, 2:5);

%i
D = B(:, 1:2);
I = eye(6,2);
E = B*I;
isequal(D, E)