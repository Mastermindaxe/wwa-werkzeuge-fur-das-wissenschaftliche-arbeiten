% Christoph Hoffmeyer   - 664349
% Florian Nagel         - 661685

function [ matrix ] = aufgabe2a( matrix, x )
%AUFGABE2A Summary of this function goes here
%   Detailed explanation goes here

for m = 1:size(matrix, 1)
    for n = 1:size(matrix, 2)
        if matrix(m,n) < 0
            matrix(m,n) = x;
        end
    end
end

end