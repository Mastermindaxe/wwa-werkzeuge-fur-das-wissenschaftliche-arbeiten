% Christoph Hoffmeyer   - 664349
% Florian Nagel         - 661685

A = [1, -3, 5; 3, -8, 10];
A = randn(1000);

tstart = tic;
aufgabe2a(A, 1);
disp(toc(tstart));

tstart = tic;
aufgabe2b(A, 1);
disp(toc(tstart));

tstart = tic;
aufgabe2c(A, 1);
disp(toc(tstart));