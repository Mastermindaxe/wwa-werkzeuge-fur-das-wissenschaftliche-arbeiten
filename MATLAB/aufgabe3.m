% Christoph Hoffmeyer   - 664349
% Florian Nagel         - 661685

%a
z1 = -1+0.3j;
z2 = 0.8 + 0.7j;

figure;
compass(z1);
hold on;
compass(z2,'y');

%b
compass(z1+z2, 'g')
compass(z1*z2, 'r')
compass(z1/z2, 'b')
compass(conj(z1), 'm')
compass(conj(z2), 'm')
compass(inv(z1), 'c')
compass(inv(z2), 'k')
hold off;
legend('z1', 'z2', 'sum', 'prod', 'quotient', 'conjugated z1', 'conj z2', 'inverse z1', 'inv z2')

%c
theta1 = angle(z1);
theta2 = angle(z2);
r1 = abs(z1);
r2 = abs(z2);
disp('magnitude of z1:')
disp(r1)
disp('magnitude of z2:')
disp(r2)
disp('phase of z1:')
disp(theta1)
disp('phase of z2:')
disp(theta2)
disp('z1 * z2 =')
disp(r1*exp(1)^(theta1*1i) * r2*exp(1)^(theta2*1i))
disp('z1 / z2 =')
disp(r1*exp(1)^(theta1*1i) / r2*exp(1)^(theta2*1i))

%d
d10 = roots([1, zeros(1,10-1), -z1]);
d20 = roots([1, zeros(1,20-1), -z1]);
e10 = roots([1, zeros(1,10-1), -z2]);
e20 = roots([1, zeros(1,20-1), -z2]);

figure;
plot(d10, 'b');
hold on;
plot(e10, 'g');
plot(e20, 'k');
plot(d20, 'm');
hold off;
legend('10th of z1', '10th of z2', '20th of z2', '20th of z1')

%e
x = angle(e10);
figure;
plot(x, real(e10));
hold on;
x = angle(e20);
plot(x,real(e20));
xlabel 'phase'
ylabel 'real'
hold off;

x = angle(d10);
figure;
plot(x, imag(d10));
hold on;
x = angle(d20);
plot(x,imag(d20));
xlabel 'phase'
ylabel 'img'
hold off;