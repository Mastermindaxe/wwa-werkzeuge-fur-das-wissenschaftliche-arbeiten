(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      9556,        274]
NotebookOptionsPosition[      8716,        245]
NotebookOutlinePosition[      9049,        260]
CellTagsIndexPosition[      9006,        257]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell["\<\
Christoph Hoffmeyer - 664349
Florian Nagel - 661685\
\>", "Input",
 CellChangeTimes->{{3.719553056914167*^9, 3.71955305892279*^9}, {
  3.719557699793707*^9, 3.719557750437758*^9}},
 FormatType->"TextForm"],

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{"Aufgabe", " ", "1.1"}]}]], "Input",
 CellChangeTimes->{{3.719553056914167*^9, 3.71955305892279*^9}, {
  3.719557699793707*^9, 3.719557715552189*^9}}],

Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{"roundTill", "[", "n_", "]"}], " ", ":=", " ", 
   RowBox[{"N", "[", 
    RowBox[{"n", ",", "6616"}], "]"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{"roundTill", "[", "Pi", "]"}], "\[IndentingNewLine]", 
 RowBox[{"roundTill", "[", 
  RowBox[{"Sqrt", "[", "2", "]"}], "]"}], "\[IndentingNewLine]", 
 RowBox[{"roundTIll", "[", "EulerE", "]"}]}], "Input",
 CellChangeTimes->{{3.719553062574342*^9, 3.719553063217531*^9}, {
  3.7195535167127037`*^9, 3.719553718306039*^9}, {3.7195543420081*^9, 
  3.719554387372485*^9}}],

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{"Aufgabe", " ", "1.2"}]}]], "Input",
 CellChangeTimes->{{3.719553724568489*^9, 3.719553731328809*^9}}],

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{"list1", " ", "=", " ", 
   RowBox[{"Table", "[", 
    RowBox[{
     RowBox[{"i", "^", "2"}], ",", " ", 
     RowBox[{"{", 
      RowBox[{"i", ",", " ", "1", ",", " ", "3"}], "}"}]}], 
    "]"}]}]}]], "Input",
 CellChangeTimes->{{3.7195537571567307`*^9, 3.71955380538634*^9}, {
  3.719553893616993*^9, 3.719553896395631*^9}}],

Cell[BoxData[
 RowBox[{"list2", " ", "=", " ", 
  RowBox[{"{", " ", 
   RowBox[{"list1", ",", " ", 
    RowBox[{"Table", "[", " ", 
     RowBox[{
      RowBox[{"Ceiling", "[", 
       RowBox[{"Pi", "*", "n"}], "]"}], " ", ",", " ", 
      RowBox[{"{", 
       RowBox[{"n", ",", " ", "1", ",", " ", "3"}], "}"}]}], "]"}], ",", " ", 
    
    RowBox[{"Table", "[", 
     RowBox[{
      RowBox[{"3", "*", "n"}], ",", " ", 
      RowBox[{"{", 
       RowBox[{"n", ",", " ", "1", ",", " ", "3"}], "}"}]}], "]"}]}], 
   "}"}]}]], "Input",
 CellChangeTimes->{{3.719553909934095*^9, 3.7195540596425657`*^9}, {
  3.719554129413125*^9, 3.7195542434384413`*^9}, {3.719554277511653*^9, 
  3.7195542778655977`*^9}, {3.719554308311289*^9, 3.719554329912775*^9}}],

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{"Aufgabe", " ", "1.3"}]}]], "Input",
 CellChangeTimes->{{3.719554420518564*^9, 3.719554424544446*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"biggestElement", "[", "l_", "]"}], " ", ":=", " ", 
  RowBox[{"Module", "[", 
   RowBox[{
    RowBox[{"{", "i", "}"}], ",", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"i", "=", "1"}], ";", "\[IndentingNewLine]", 
     RowBox[{"While", "[", 
      RowBox[{
       RowBox[{"i", "<", " ", 
        RowBox[{"Length", "[", "l", "]"}]}], ",", 
       "\[IndentingNewLine]"}]}]}]}]}]}]], "Input",
 CellChangeTimes->{{3.719554445810565*^9, 3.719554471569549*^9}, {
  3.719554567554538*^9, 3.719554567858562*^9}, {3.7195546372449827`*^9, 
  3.719554689588142*^9}},
 EmphasizeSyntaxErrors->True],

Cell[BoxData[
 RowBox[{"Aufgabe", " ", "1.4"}]], "Input",
 CellChangeTimes->{{3.7195548419051247`*^9, 3.719554846988389*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"aufgabe4", "[", 
    RowBox[{"n_", ",", " ", "k_"}], "]"}], " ", ":=", " ", 
   RowBox[{
    RowBox[{"Factorial", "[", "n", "]"}], "/", 
    RowBox[{"(", " ", 
     RowBox[{
      RowBox[{"Factorial", "[", "k", "]"}], "*", 
      RowBox[{"Factorial", "[", 
       RowBox[{"n", "-", "k"}], "]"}]}], " ", ")"}]}]}], ";"}]], "Input",
 CellChangeTimes->{{3.719554925147109*^9, 3.7195550124158916`*^9}}],

Cell[BoxData[
 RowBox[{"Column", "[", 
  RowBox[{
   RowBox[{"Table", "[", 
    RowBox[{
     RowBox[{"aufgabe4", "[", 
      RowBox[{"n", " ", ",", " ", "k"}], "]"}], ",", 
     RowBox[{"{", 
      RowBox[{"n", ",", "0", ",", "10"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"k", ",", "0", ",", "n"}], "}"}]}], "]"}], ",", "Center"}], 
  "]"}]], "Input",
 CellChangeTimes->{{3.719555180890764*^9, 3.719555195097836*^9}, {
  3.719555243306275*^9, 3.719555284562304*^9}}],

Cell[BoxData[
 RowBox[{"Aufgabe", " ", "1.5"}]], "Input",
 CellChangeTimes->{{3.719555279039097*^9, 3.719555296454563*^9}}],

Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{"aufgabe5", "[", "n_", "]"}], " ", ":=", 
   RowBox[{"Module", "[", 
    RowBox[{
     RowBox[{"{", "x", "}"}], ",", "\[IndentingNewLine]", 
     RowBox[{
      RowBox[{"x", "=", "0"}], ";", "\[IndentingNewLine]", 
      RowBox[{"For", "[", 
       RowBox[{
        RowBox[{"i", "=", "1"}], ",", " ", 
        RowBox[{"i", "<", "n"}], ",", 
        RowBox[{"i", "++"}], ",", "\[IndentingNewLine]", 
        RowBox[{"For", "[", 
         RowBox[{
          RowBox[{"j", "=", "1"}], ",", 
          RowBox[{"j", "<", "n"}], ",", 
          RowBox[{"j", "++"}], ",", "\[IndentingNewLine]", 
          RowBox[{"x", "=", 
           RowBox[{"x", "+", 
            RowBox[{"i", "*", "j"}]}]}]}], "]"}]}], "\[IndentingNewLine]", 
       "]"}]}]}], "\[IndentingNewLine]", "]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{"Timing", "[", 
  RowBox[{"aufgabe5", "[", "1000", "]"}], "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"aufgabe5a", "[", "n_", "]"}], " ", ":=", " ", 
   "\[IndentingNewLine]", 
   RowBox[{"Sum", "[", 
    RowBox[{
     RowBox[{"Sum", "[", 
      RowBox[{
       RowBox[{"i", "*", "j"}], ",", " ", 
       RowBox[{"{", 
        RowBox[{"j", ",", " ", "1", ",", " ", "1000"}], "}"}]}], "]"}], ",", 
     RowBox[{"{", 
      RowBox[{"i", ",", " ", "1", ",", " ", "1000"}], "}"}]}], "]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{"Timing", "[", 
  RowBox[{"aufgabe5a", "[", "1000", "]"}], "]"}]}], "Input",
 CellChangeTimes->{{3.719555345018538*^9, 3.719555546815464*^9}, {
  3.719555595436796*^9, 3.7195556466138277`*^9}, {3.719555729055622*^9, 
  3.7195558189838953`*^9}, {3.7195559292071333`*^9, 3.719555932220118*^9}, {
  3.719555978843898*^9, 3.719555994590105*^9}, {3.7195560435464287`*^9, 
  3.719556049410849*^9}, {3.7195561519089212`*^9, 3.7195561524957933`*^9}, {
  3.719556217953085*^9, 3.7195562421246023`*^9}, {3.71955627303727*^9, 
  3.71955630755387*^9}, {3.719556340685851*^9, 3.7195563749913483`*^9}}],

Cell[BoxData[
 RowBox[{"Aufgabe", " ", "1.6"}]], "Input",
 CellChangeTimes->{{3.7195558054022303`*^9, 3.71955581181176*^9}}],

Cell[BoxData[
 RowBox[{"Do", "[", 
  RowBox[{
   RowBox[{"If", "[", 
    RowBox[{
     RowBox[{"Element", "[", 
      RowBox[{"n", ",", " ", "Primes"}], "]"}], ",", " ", 
     RowBox[{"Print", "[", 
      RowBox[{"n", ",", "\"\<Primzahlzwilling\>\""}], "]"}], ",", " ", 
     RowBox[{"Print", "[", "n", "]"}]}], "]"}], ",", " ", 
   RowBox[{"{", 
    RowBox[{"n", ",", " ", "1", ",", " ", "100"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.719556744205855*^9, 3.719556852812689*^9}, {
  3.7195568831203117`*^9, 3.719556927564994*^9}}],

Cell[BoxData[
 RowBox[{"Aufgabe", " ", "1.7"}]], "Input",
 CellChangeTimes->{{3.719556962003932*^9, 3.719556965216083*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{"numPrime", "[", "n_", "]"}], " ", ":=", " ", 
     RowBox[{"Module", "[", 
      RowBox[{
       RowBox[{"{", "x", "}"}], ",", "\[IndentingNewLine]", 
       RowBox[{"While", "[", 
        RowBox[{
         RowBox[{"n", "\[NotEqual]", " ", "x"}], ",", "\[IndentingNewLine]", 
         RowBox[{"x", "=", 
          RowBox[{"x", "+", "1"}]}]}], "\[IndentingNewLine]", "]"}]}], 
      "\[IndentingNewLine]", "]"}]}], "\[IndentingNewLine]"}], "]"}], "/;", 
  " ", 
  RowBox[{"Element", "[", 
   RowBox[{"n", ",", " ", "Primes"}], "]"}]}]], "Input",
 CellChangeTimes->{{3.719556999927092*^9, 3.719557014917062*^9}, {
   3.719557075482748*^9, 3.719557110054372*^9}, 3.719557140418755*^9, {
   3.7195571925081244`*^9, 3.719557200972446*^9}, {3.719557320066987*^9, 
   3.7195573202688313`*^9}, {3.7195573565611277`*^9, 3.719557436194223*^9}, {
   3.719557501466661*^9, 3.7195575193699827`*^9}, 3.7195575550753813`*^9, {
   3.719557602931717*^9, 3.719557605048729*^9}, {3.7195576514131393`*^9, 
   3.7195576535624037`*^9}}]
},
WindowSize->{958, 1150},
WindowMargins->{{Automatic, 0}, {Automatic, 0}},
FrontEndVersion->"10.0 for Linux x86 (64-bit) (June 27, 2014)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 215, 6, 50, "Input"],
Cell[776, 28, 207, 4, 55, "Input"],
Cell[986, 34, 565, 12, 99, "Input"],
Cell[1554, 48, 159, 3, 55, "Input"],
Cell[1716, 53, 382, 10, 55, "Input"],
Cell[2101, 65, 748, 19, 32, "Input"],
Cell[2852, 86, 159, 3, 55, "Input"],
Cell[3014, 91, 627, 16, 99, "Input"],
Cell[3644, 109, 125, 2, 32, "Input"],
Cell[3772, 113, 445, 12, 32, "Input"],
Cell[4220, 127, 475, 13, 32, "Input"],
Cell[4698, 142, 123, 2, 32, "Input"],
Cell[4824, 146, 2001, 48, 253, "Input"],
Cell[6828, 196, 124, 2, 32, "Input"],
Cell[6955, 200, 540, 13, 32, "Input"],
Cell[7498, 215, 123, 2, 32, "Input"],
Cell[7624, 219, 1088, 24, 143, "Input"]
}
]
*)

(* End of internal cache information *)
